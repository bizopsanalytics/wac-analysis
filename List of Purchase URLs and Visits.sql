select path, count(distinct session_id)
from zone_marketing.rich_pages
where path like '%/purchase/%'
and date between '2016-12-01' and '2016-12-31'
and hostname = 'www.atlassian.com'
and country = 'Japan'
group by 1
order by 2 desc

;
select * 
from zone_marketing.rich_pages
limit 20