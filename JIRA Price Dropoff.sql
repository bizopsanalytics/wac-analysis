--designed to calculate the number of customers dropping off after viewing prices.

with jira_price as
(
select  distinct country, 
                 session_id
from zone_marketing.rich_pages
where path = '/purchase/product/jira-software'
and date between '2016-07-01' and '2016-12-31'
and hostname = 'www.atlassian.com'
--and country in ('Japan', 'Australia', 'Germany', 'United States')
group by 1,2
),
cart_add as 
(
select  distinct session_id
from zone_marketing.rich_pages
where path = '/purchase/cart'
and date between '2016-07-01' and '2016-12-31'
and hostname = 'www.atlassian.com'
--and country in ('Japan', 'Australia', 'Germany', 'United States')
group by 1
)
select  country, 
        count(a.session_id) as jira_price, 
        count(b.session_id) as cart_add
from jira_price as a
left join cart_add as b on a.session_id = b.session_id
group by 1
order by 2 desc


